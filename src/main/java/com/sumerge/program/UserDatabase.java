package com.sumerge.program;



import javax.enterprise.context.RequestScoped;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("users")
public class UserDatabase {





    private static final Logger LOGGER = Logger.getLogger(UserDatabase.class.getName());
    List<User> Users = new ArrayList<User>(Arrays.asList(new User(1, "Ahmed", "Ahmed@sumerge.com"),
            new User(2, "Adel", "Adel@sumerge.com"),
            new User(3, "Sama", "Sama@sumerge.com")
            ));

    @Context
    HttpServletRequest request;

    @GET
    public Response ShowAll() {
        try {
            return Response.ok().
                    entity(this.Users).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }

    }


    @DELETE
    @Path("{id}")
    public Response deleteStudent(@PathParam("id") Long id){
        try {
                for ( int i =0 ;i < Users.size() ; i ++ )
                {
                    if (id == Users.get(i).getID())
                        this.Users.remove(i);
                }

            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }


    }

    @GET
    @Path("{id}")
    public Response getUser(@PathParam("id") Long id) {
        try {
            User returnUser = new User();
            for (int i = 0; i < Users.size(); i++) {
                if (id == Users.get(i).getID())
                    returnUser =  this.Users.get(i);
            }
            return Response.ok().
                    entity(returnUser).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
}