package com.sumerge.program;

public class User {

    private
    int ID ;
    String name ;
    String emailAddress;

    public User()
    {

    }

    public User(int ID, String name, String emailAddress) {
        this.ID = ID;
        this.name = name;
        this.emailAddress = emailAddress;
    }


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Override
    public String toString() {
        return "User{" +
                "ID=" + ID +
                ", name='" + name + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                '}';
    }
}
