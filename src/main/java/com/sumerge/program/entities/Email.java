package com.sumerge.program.entities;


import javax.persistence.*;

@Table(name="email")
@Entity
public class Email {

    @Id
    @Column(name="EMAILID")
    int emailID;


    @OneToOne
    @JoinColumn(name="EMPID")
    employee empID;

}
