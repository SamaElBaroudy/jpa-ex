package com.sumerge.program.entities;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Table(name="project")
@Entity
public class Project {

    @Id
    @Column(name="PROJID")
    String projectID;

    @Column(name="PROJNAME")
    String projectName;

    @Column(name="STARTDATE")
    @Temporal(TemporalType.DATE)
    Date startDate;

    @Column(name="TARGETDATE")
    @Temporal(TemporalType.DATE)
    Date targetDate;

    @Column(name="STATUS")
    String status;

    @Column(name="DESCRIPTION")
    String description;

    @ManyToMany(mappedBy = "projects")
    List<employee> employees;



}
