package com.sumerge.program.entities;

import javax.persistence.*;


@Table(name="photo")
@Entity
public class Photo {

    @Id
    @Column(name="PHOTOID")
    int photoID;

    @ManyToOne
    @Column(name="EMPID")
    employee emp;

    @Column(name="IMAGENAME")
    String imageName;

}
