package com.sumerge.program.entities;

import javax.persistence.*;


@Table(name ="address")
@Entity
public class address{

    @Id
    @Column(name="ADDRESSID")
    int addressID ;

    @OneToOne
    @JoinColumn(name="EMPID")
    employee emp ;

    @Column(name="ADDLINE1")
    String addLine1;

    @Column(name="ADDLINE2")
    String addLine2;

    @Column(name="CITY")
    String city;

    @Column(name="REGION")
    String region ;

    @Column(name="COUNTRY")
    String country;

    @Column(name="POSTCODE")
    String postCode;

}
