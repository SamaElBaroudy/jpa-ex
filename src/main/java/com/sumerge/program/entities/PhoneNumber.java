package com.sumerge.program.entities;

import javax.persistence.*;


@Table(name="phonenumber")
@Entity
public class PhoneNumber {

    @Id
    @Column(name="PHONEID")
    int phoneID;

    @OneToOne
    @JoinColumn(name ="EMPID")
    employee empID;

    @Column(name ="LOCALNUM")
    String localnum ;

    @Column(name="INTLPREFIX")
    String intlprefix;

    @Column(name="PHONETYPE")
    String phoneType;

}
