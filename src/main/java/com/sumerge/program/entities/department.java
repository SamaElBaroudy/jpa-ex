package com.sumerge.program.entities;


import com.sun.javafx.beans.IDProperty;

import javax.persistence.*;
import java.util.List;

@Table(name="department")
@Entity
public class department {

    @OneToMany
    @JoinColumn(name="EMPID")
    List<employee> manager ;

    @Column(name="DEPTNAME")
    String  departname;

    @Id
    @Column(name="DEPTCODE")
    String departcode;


}
