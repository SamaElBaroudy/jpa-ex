package com.sumerge.program.entities;

import javax.persistence.*;
import java.util.List;

@Table(name="employee")
@Entity
public class employee {

    @Id
    @Column(name="EMPID")
    String empID;

    @OneToOne
    @JoinColumn(name="DEPTCODE")
    department departmentCode;

    @Column(name="JOBTITLE")
    String jobTitle;

    @Column(name="GIVINNAME")
    String givenName;

    @Column(name="FAMILYNAME")
    String familyName;

    @Column(name="COMMONNAME")
    String commonName;

    @Column(name="NAMETITLE")
    String nameTitle ;

    @ManyToMany
    @JoinTable(
            name = "projectmember",
            joinColumns = @JoinColumn(name = "EMPID"),
            inverseJoinColumns = @JoinColumn(name = "PROJID"))
    List<Project> projects;

}
